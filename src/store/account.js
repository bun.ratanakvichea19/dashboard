import { db, auth } from '../main'
import BDError from '../libs/error'

export default {
  namespaced: true,
  state: {
    user: {}, // only contains value if the user is authenticated
    lang: '',
    error: null
  },
  mutations: {
    user (state, payload) {
      if (!payload) {
        state.user = null
      } else {
        state.user = payload
      }
    },
    lang (state, payload) {
      if (!payload) { return }
      state.lang = payload
    },
    error (state, payload) {
      state.error = payload
    }
  },
  actions: {
    checkShop: function ({ state, rootState, commit, dispatch, getters, rootGetters }) {
      const restaurantRef = db.collection('restaurants')
      const userRef = db.collection('users')
      return new Promise((resolve, reject) => {
        commit('error', null)
        if (!auth.currentUser) { return reject(new BDError('account', 'user not authenticated')) }
        const uid = auth.currentUser.uid
        restaurantRef.where('managedBy', '==', userRef.doc(uid))
          .get().then(querySnap => {
            const hasShop = !querySnap.empty
            commit('hasShop', hasShop)
            return resolve(hasShop)
          }).catch(error => {
            commit('hasShop', false)
            commit('error', error)
            return reject(error)
          })
      })
    },

    createAccount: function ({ state, rootState, commit, dispatch, getters, rootGetters }) {
      return new Promise((resolve, reject) => {
        commit('error', null)
        const currentUser = auth.currentUser
        console.log('curuser', currentUser)
        const userRef = db.collection('users').doc(currentUser.uid)
        console.log('userref', userRef)
      })
    },

    clearError: function ({ state, rootState, commit, dispatch, getters, rootGetters }) {
      commit('error', null)
    },

    /**
     * @func
     * @dec clear firebase auth
     *  The consequences of this will lead to
     *  autoSignIn Invocation from firebase auth watcher
     */
    signOut: function ({ state, rootState, commit, dispatch, getters, rootGetters }) {
      auth.signOut().then(() => {
        commit('user', null)
        commit('error', null)
        commit('shop', null)
        commit('hasShop', false)
        console.log('sign out success')
      }).catch(error => {
        console.error(error.message)
        commit('error', error)
      })
    },

    /**
     * @func
     * @desc autoSignIn will re-query user's data on reload, if route is sign-up, similar function will be revoked manually
     */
    autoSignIn: function ({ state, rootState, commit, dispatch, getters, rootGetters }) {
      return new Promise((resolve, reject) => {
        const uid = auth.currentUser.uid
        console.log('we have user here', uid)
        const userRef = db.collection('users').doc(uid)
        const promises = []
        promises.push(userRef.get())
        Promise.all(promises)
          .then(results => {
            console.log('result', results[0].exists, results[1].empty)
            commit('user', results[0].data())
            return resolve()
          }).catch(error => {
            console.error('error', error.message)
            commit('error', error)
            return reject(error)
          })
      })
    }
  },
  getters: {
    lang: function (state) {
      if (state.lang) {
        return state.lang
      } else {
        let returningLang = ''
        if (navigator.languages !== undefined) {
          console.log('navlang', navigator.language[0])
          returningLang = navigator.languages[0]
        } else {
          returningLang = navigator.language
        }
        if (returningLang) {
          if (returningLang.includes('-')) { returningLang = returningLang.split('-')[0] }
          return returningLang
        } else return 'en'
      }
    }
  }

}
