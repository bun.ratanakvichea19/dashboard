import Vue from 'vue'
import Vuex from 'vuex'
import Account from './account'
import { db } from '../main'

Vue.use(Vuex)
const state = {
  appName: 'EOrder Dashboard',
  shortAppName: 'EOrder',
  db: db
}
const mutations = {}
const actions = {}
const getters = {}
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules: { account: Account }
})
