import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import AppLogin from '../components/AppLogin'
import Employee from '../components/main/Employee'
import Order from '../components/main/Order'
import Payment from '../components/main/Payment'
import Product from '../components/main/Product'
import Setting from '../components/main/Setting'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/Login',
      name: 'AppLogin',
      component: AppLogin
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/Employee',
      name: 'Employee',
      component: Employee
    },
    {
      path: '/Order',
      name: 'Order',
      component: Order
    },
    {
      path: '/Payment',
      name: 'Payment',
      component: Payment
    },
    {
      path: '/Product',
      name: 'Product',
      component: Product
    },
    {
      path: '/Setting',
      name: 'Setting',
      component: Setting
    }
  ]
})
