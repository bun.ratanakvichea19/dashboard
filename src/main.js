import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import store from './store/index'
// import RedirectInitRoute from './libs/redirect_initial_route'

import config from './config'
import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
firebase.initializeApp(config.firebaseConfig)

Vue.prototype.$firebase = firebase
Vue.prototype.$firebaseAuth = firebase.auth()
Vue.prototype.$firebaseDb = firebase.firestore()

export const db = firebase.firestore()
export const auth = firebase.auth()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App),
  mounted: function () {
    console.log('none')
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log('user', user)
        this.$router.push({ path: '/' })
      } else {
        console.log('push to login')
        this.$router.push({ path: '/login' })
      }
    })
  }
}).$mount('#app')
