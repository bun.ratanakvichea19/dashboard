const session = window.sessionStorage
const INITIAL_PATH = 'initial_path'
const IS_PUSH_INITIAL = 'push_initial_path'
const RedirectInitialRoute = {
  /**
   * @function
   * @desc to init the path to be redirected
   * @param pathName
   */
  init: (pathName) => {
    session.setItem(INITIAL_PATH, pathName)
    session.setItem(IS_PUSH_INITIAL, 'false')
  },
  /**
   * @function
   * @desc redirect route based on previous redirect route
   * @param router
   * @param fallBackPath
   * @return {*}
   */
  redirect: (router, fallBackPath) => {
    if (session.getItem(IS_PUSH_INITIAL) === 'false' && session.getItem(INITIAL_PATH)) {
      session.setItem(IS_PUSH_INITIAL, 'true')
      router.push({ name: session.getItem(INITIAL_PATH) })
      return session.removeItem(INITIAL_PATH)
    }
    return router.push({ name: fallBackPath })
  }
}
export default RedirectInitialRoute
