/**
 * @file error.js
 * @desc
 *   BodiaTech Co. Prvider@E-order <br>
 *   Date: July 10th, 2019 <br>
 *   This file is subject to the terms and conditions defined in
 *   file 'LICENSE.txt', which is part of this source code package.
 *   SSRError is a custom error class which extends the standard error.
 *   This is define the title, message and bodiatech error code (bdCode)
 *   Any error throws from this project shall extend this error class first.
 * @author Chhorm Chhatra <ch.chhatra@gmail.com>
 * @copyright BodiaTech Co.
 * @version 1.0.0
 */

export default class BDError extends Error {
  get title () {
    return this._title
  }

  get message () {
    return this._message
  }

  // bdCode is the code assigned specifically by bodiatech co. only
  get bdCode () {
    return this._bdCode
  }

  constructor (title, message, bdCode) {
    super()
    this._title = title
    this._message = message
    this._bdCode = bdCode
  }
}
