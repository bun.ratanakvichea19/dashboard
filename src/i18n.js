import Vue from 'vue'
import VueI18n from 'vue-i18n'
import store from './store/index'

Vue.use(VueI18n)

console.log('default lang to set', store.getters['account/lang'])
const i18n = new VueI18n({
  locale: store.getters['account/lang'],
  messages: {
    en: require('./locales/en'),
    km: require('./locales/km')
  },
  fallbackLocale: 'km',
  silentFallbackWarn: true
})

if (module.hot) {
  module.hot.accept(['./locales/en', './locales/km'], () => {
    i18n.setLocaleMessage('en', require('./locales/en'))
    i18n.setLocaleMessage('km', require('./locales/km'))
  })
}
export default i18n

// function loadLocaleMessages () {
//   const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
//   const messages = {}
//   locales.keys().forEach(key => {
//     const matched = key.match(/([A-Za-z0-9-_]+)\./i)
//     if (matched && matched.length > 1) {
//       const locale = matched[1]
//       messages[locale] = locales(key)
//     }
//   })
//   return messages
// }
//
// export default new VueI18n({
//   locale: process.env.VUE_APP_I18N_LOCALE || 'en',
//   fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
//   messages: loadLocaleMessages()
// })
