import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import i18n from 'vue-i18n'
import LangSwitcher from '../components/LangSwitcher'
import NavDrawer from '../components/NavDrawer'

Vue.component('v-lang-switcher', LangSwitcher)
Vue.component('v-nav-drawer', NavDrawer)

Vue.use(Vuetify)

export default new Vuetify({
  iconfont: 'md',
  lang: {
    t: (key, ...params) => i18n.t(key, params)
  },
  theme: {
    background: '#F2F2F2',
    primary1: '#005E9B',
    secondary1: '#5CC2E8',
    secondary2: '#63E2C6',
    white: '#FFFFFF',
    red: '#EB5757',
    yellow: '#F2C94C'
  }
})
